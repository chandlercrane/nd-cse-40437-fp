
import pandas as pd

if __name__ == "__main__":

	currentDF = pd.read_csv('../data/NYPD_Arrest_Data__Year_to_Date_.csv')
	currentDF.to_json('../data/arrestData.json', orient='records')
