# Final Project
# Name: Chandler Crane

# Imports
import sys
import tweepy
import json
from datetime import datetime
from tweepy import OAuthHandler

# Global
FILENAME = './tweets.json'

# Body
def setup():
    f = open("keys.txt", "r")
    keys = f.read().split('\n')
    consumer_key = keys[0]
    consumer_secret = keys[1]
    auth = OAuthHandler(consumer_key, consumer_secret)

    access_token = keys[2]
    access_token_secret = keys[3]
    auth.set_access_token(access_token, access_token_secret)
    return auth

def readData():
    try:
        with open(FILENAME) as json_file:
            data = json.load(json_file)
            return data
    except:
        tweets = {}
        tweets['tweets'] = []
        tweets['scrapes'] = []
        tweets['duplicates'] = 0
        return tweets

def writeData(data):
    with open(FILENAME, 'w') as outfile:
        json.dump(data, outfile)

def performSearch(query, region, LIMIT=50):
    tweets_badformat = api.search(q=query, geocode=region, count=LIMIT, tweet_mode="extended")
    tweets = []
    for t in tweets_badformat:
        tweets.append(t._json)
    return tweets

def joinTweets(old_tweets, new_tweets):
    tweets = old_tweets.copy()

    time = datetime.now()

    scrapedata = {
        'duplicates': 0,
        'month': time.strftime("%m"),
        'day': time.strftime("%d"),
        'year': time.strftime("%Y"),
        'hour': time.strftime("%H"),
        'minute': time.strftime("%M"),
        'second': time.strftime("%S"),
    }

    for t in new_tweets:
        if t not in tweets['tweets']:
            # adding new tweet...
            tweets['tweets'].append(t)
        else:
            # duplicate detected
            scrapedata['duplicates'] += 1

    tweets['scrapes'].append(scrapedata)
    tweets['duplicates'] += scrapedata['duplicates']

    return tweets

if __name__ == "__main__":

    # Read in existing data...
    pretweets = readData()

    auth = setup()
    api = tweepy.API(auth)
    query = "crime+OR+arrest+OR+danger+OR+traffic+OR+accident+OR+scene+OR+police+OR+emergency+OR+felony+OR+misdemeanor"
    region = "40.755,-73.985,5mi"

    newtweets = performSearch(query, region)

    tweets = joinTweets(pretweets, newtweets)
    print(tweets['tweets'])
    writeData(tweets)
