mapboxgl.accessToken = 'pk.eyJ1IjoiY2hhbmRsZXJjcmFuZSIsImEiOiJjanQwcTNhaWEwM2x2M3pwM2I4Nm40Nmo2In0.82_w067nYHhuvrsL3kuHUw';
var map = new mapboxgl.Map({
  container: 'map', // container id
  //style: 'mapbox://styles/chandlercrane/cjt0qcy7d01ya1fmmcdis1lis',
  style: 'mapbox://styles/chandlercrane/ck85e6krw02ry1jlyunpslbk1',
  center: [-73.95, 40.71], // starting position [lng, lat]
  zoom: 9 // starting zoom
});

var size = 40;

var blank_source = {"type": "FeatureCollection", "features": []}
var global_tweet_source = blank_source
var global_arrest_source = blank_source
var global_covid_source = blank_source

// Get DOM Elements
const arrestToggle = document.getElementById("arrestToggle")
const tweetToggle = document.getElementById("tweetToggle")
const covidToggle = document.getElementById("covidToggle")

arrestToggle.addEventListener('click', (e) => {
  if(arrestToggle.classList.contains('a-on')){
    arrestToggle.classList.remove('a-on')
    arrestToggle.classList.add('a-off')
    arrestToggle.innerText = "Arrest Off"
    map.getSource('arrest-data').setData(blank_source)
  }
  else{
    arrestToggle.classList.add('a-on')
    arrestToggle.classList.remove('a-off')
    arrestToggle.innerText = "Arrest On"
    map.getSource('arrest-data').setData(global_arrest_source)
  }
})
tweetToggle.addEventListener('click', (e) => {
  if(tweetToggle.classList.contains('t-on')){
    tweetToggle.classList.remove('t-on')
    tweetToggle.classList.add('t-off')
    tweetToggle.innerText = "Tweet Off"
    map.getSource('tweet-data').setData(blank_source)
  }
  else{
    tweetToggle.classList.add('t-on')
    tweetToggle.classList.remove('t-off')
    tweetToggle.innerText = "Tweet On"
    map.getSource('tweet-data').setData(global_tweet_source)
  }
})
covidToggle.addEventListener('click', (e) => {
  if(covidToggle.classList.contains('c-on')){
    covidToggle.classList.remove('c-on')
    covidToggle.classList.add('c-off')
    covidToggle.innerText = "COVID Off"
    map.getSource('covid-data').setData(blank_source)
  }
  else{
    covidToggle.classList.add('c-on')
    covidToggle.classList.remove('c-off')
    covidToggle.innerText = "COVID On"
    map.getSource('covid-data').setData(global_covid_source)
  }
})

const totalCases = document.getElementById("totalCases")
const totalViolent = document.getElementById("totalViolent")

const totalTested = document.getElementById("totalTested")
const totalPositive = document.getElementById("totalPositive")

const totalTweets = document.getElementById("totalTweets")

var arrest_num_cases = 0
var arrest_num_violent = 0
var covid_num_tested = 0
var covid_num_positive = 0
var tweet_num_total = 0

var zip_dict = {}
var currentPopups = []

const zipInput = document.getElementById("zipInput")

zipInput.addEventListener('keypress', (e) => {
  //console.log(e.key)
  //console.log(zipInput.value)
  zip = zipInput.value.toString() + e.key.toString()
  //console.log('true zip', zip)
  if(zip in zip_dict){
    removePopups()
    createPopup(zip)
  }
})

function createPopup(zip){
  e = zip_dict[zip]
  var positive = e.properties.positive
  var total = e.properties.total
  var percentage = e.properties.percentage
  var lat = e.properties.lat
  var lon = e.properties.lon
  var zip = e.properties.zip
  var coordinates = e.geometry.coordinates.slice()

  var html = "<h1>" + zip + "</h1>"
  html += "<p>Positive: "+ positive + "</br>"
  html += "Total: "+ total + "</br>"
  html += "Percentage: "+ percentage + "%</p>"
  var popup = new mapboxgl.Popup()
    .setLngLat(coordinates)
    .setHTML(html)
    .addTo(map)
  currentPopups.push(popup)
  //console.log(currentPopups)

  // Clear out the input
  //zipInput.value = ''
}

function removePopups(){
  for(p in currentPopups){
    //console.log(currentPopups[p])
    currentPopups[p].remove()
  }
  currentPopups = []
}

function getPoints(){
  getArrestPoints()
  getCovidPoints()
  getTweetPoints()
}

function getCovidPoints(){
  //var dp = getFakeDataCovid()
  fetch('./data/med-covid-data.json')
    .then((response) => {
      return response.json();
    })
    .then((dps) => {
      console.log(dps)
      //covid_data_points.source.data.features = dps.features
      tT = 0
      tP = 0

      for(p in dps.features){
        zip_dict[dps.features[p]['properties']['zip']] = dps.features[p]
        tT += dps.features[p]['properties']['total']
        tP += dps.features[p]['properties']['positive']
      }
      covid_num_tested = tT
      covid_num_positive = tP
      totalTested.innerText = covid_num_tested
      totalPositive.innerText = covid_num_positive
      global_covid_source = dps
      map.getSource('covid-data').setData(dps)

    });
}

function getTweetPoints(){
  fetch('./data/med-tweet-data.json')
    .then((response) => {
      return response.json();
    })
    .then((dps) => {
      console.log(dps)
      //tweet_data_points.source.data.features = dps.features
      tT = 0
      for(p in dps.features){
        tT += 1
      }
      tweet_num_total = tT
      totalTweets.innerText = tweet_num_total

      global_tweet_source = dps
      map.getSource('tweet-data').setData(dps)

    });
}

function getArrestPoints(){
  //var dp = getFakeDataArrest()
  //arrest_data_points.source.data.features = dp['features']
  //console.log(dp['features'].length)

  fetch('./data/med-arrest-data.json')
  .then(response => response.json())
  .then(dps => {
    console.log(dps)
    updateArrest(dps)
  })
}

function updateArrest(dps){
  console.log("in here")
  console.log(dps)
  violent_codes = ["130", "240", "120", "121", "215", "265", "405", "490", "125", "140", "160", "135", "150", "255"]
  //arrest_data_points.source.data.features = dps.features
  nV = 0
  for(c in dps.features){
    code = dps.features[c].properties.code
    //console.log(code)
    if(code.slice(0,3) == "VTL"){
      continue
    }
    else if(code.split(" ").length < 2){
      continue
    }
    else if(violent_codes.includes(code.split(" ")[1].slice(0,3))){
      nV += 1
    }
  }
  arrest_num_cases = dps.features.length
  arrest_num_violent = nV
  totalCases.innerText = arrest_num_cases
  totalViolent.innerText = arrest_num_violent
  //map.render()

  global_arrest_source = dps
  map.getSource('arrest-data').setData(dps)
  //arrest_source.setData(z)
}

var size_lg = size
var arrest_dot = {
  width: size,
  height: size,
  data: new Uint8Array(size * size * 4),

  onAdd: function() {
    var canvas = document.createElement('canvas');
    canvas.width = this.width;
    canvas.height = this.height;
    this.context = canvas.getContext('2d');
  },

  render: function() {
    var duration = 1000;
    var t = (performance.now() % duration) / duration;

    var radius = size / 2 * .8;
    var outerRadius = size / 2 * 0.7 * t + radius;
    var context = this.context;

    // draw inner circle
    context.beginPath();
    context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);

    // *** POINT COLOR STYLING ***
    context.fillStyle = '#C70039'; // arrest
    //context.fillStyle = '#FFDD05'; // covid
    //context.fillStyle = '#1DA1F2'; // tweet

    context.strokeStyle = 'white' //'#FF5733';
    context.lineWidth = 2;
    context.fill();
    context.stroke();
    // update this image's data with data from the canvas
    this.data = context.getImageData(0, 0, this.width, this.height).data;
    // keep the map repainting
    map.triggerRepaint();
    // return `true` to let the map know that the image was updated
    return true;
  }
};
var covid_dot = {
  width: size_lg,
  height: size_lg,
  data: new Uint8Array(size_lg * size_lg * 4),

  onAdd: function() {
    var canvas = document.createElement('canvas');
    canvas.width = this.width;
    canvas.height = this.height;
    this.context = canvas.getContext('2d');
  },

  render: function() {
    var duration = 1000;
    var t = (performance.now() % duration) / duration;

    var radius = size_lg / 2 * .8;
    var outerRadius = size_lg / 2 * 0.7 * t + radius;
    var context = this.context;

    // draw inner circle
    context.beginPath();
    context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);

    // *** POINT COLOR STYLING ***
    //context.fillStyle = '#C70039'; // arrest
    context.fillStyle = '#FFDD05'; // covid
    //context.fillStyle = '#1DA1F2'; // tweet

    context.strokeStyle = 'white' //'#ED008C';
    context.lineWidth = 2;
    context.fill();
    context.stroke();
    // update this image's data with data from the canvas
    this.data = context.getImageData(0, 0, this.width, this.height).data;
    // keep the map repainting
    map.triggerRepaint();
    // return `true` to let the map know that the image was updated
    return true;
  }
};
var tweet_dot = {
  width: size,
  height: size,
  data: new Uint8Array(size * size * 4),

  onAdd: function() {
    var canvas = document.createElement('canvas');
    canvas.width = this.width;
    canvas.height = this.height;
    this.context = canvas.getContext('2d');
  },

  render: function() {
    var duration = 1000;
    var t = (performance.now() % duration) / duration;

    var radius = size / 2 * .8;
    var outerRadius = size / 2 * 0.7 * t + radius;
    var context = this.context;

    // draw inner circle
    context.beginPath();
    context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);

    // *** POINT COLOR STYLING ***
    //context.fillStyle = '#C70039'; // arrest
    //context.fillStyle = '#FFDD05'; // covid
    context.fillStyle = '#1DA1F2'; // tweet

    context.strokeStyle = 'white' //'#1DB954';
    context.lineWidth = 2;
    context.fill();
    context.stroke();
    // update this image's data with data from the canvas
    this.data = context.getImageData(0, 0, this.width, this.height).data;
    // keep the map repainting
    map.triggerRepaint();
    // return `true` to let the map know that the image was updated
    return true;
  }
};

var arrest_data_points = {
  "id": "arrest-dot",
  "type": "symbol",
  "source": "arrest-data",
  "layout": {
    //"icon-allow-overlap": true,
    "icon-image": "arrest_dot"
  }
}
var covid_data_points = {
  "id": "covid-dot",
  "type": "symbol",
  "source": "covid-data",
  "layout": {
    //"icon-allow-overlap": true,
    "icon-image": "covid_dot"
  }
}
var tweet_data_points = {
  "id": "tweet-dot",
  "type": "symbol",
  "source": "tweet-data",
  "layout": {
    //"icon-allow-overlap": true,
    "icon-image": "tweet_dot"
  }
}

var arrest_source = {
  type: 'geojson',
  data: {
    type: "FeatureCollection",
    features: []
  }
}
var covid_source = {
  type: 'geojson',
  data: {
    type: "FeatureCollection",
    features: []
  }
}
var tweet_source = {
  type: 'geojson',
  data: {
    type: "FeatureCollection",
    features: []
  }
}

map.on('load', function () {
  map.addImage("arrest_dot", arrest_dot, { pixelRatio: 2 });
  map.addImage("covid_dot", covid_dot, { pixelRatio: 2 });
  map.addImage("tweet_dot", tweet_dot, { pixelRatio: 2 });

  map.addSource("arrest-data", arrest_source)
  map.addSource("covid-data", covid_source)
  map.addSource("tweet-data", tweet_source)

  map.addLayer(arrest_data_points)
  map.addLayer(covid_data_points)
  map.addLayer(tweet_data_points)

  getPoints()

  // When a click event occurs on a feature in the places layer, open a popup at the
  // location of the feature, with description HTML from its properties.
  map.on('click', 'tweet-dot', function (e) {
    var user = e.features[0].properties.user
    var location = e.features[0].properties.location
    var text = e.features[0].properties.text
    var lat = e.features[0].properties.lat
    var lon = e.features[0].properties.lon
    var coordinates = e.features[0].geometry.coordinates.slice()

    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
      coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    var html = "<h3>@" + user + "</h3>"
    html += "<p>Location: "+ location + "</br>"
    html += "Details: "+ text + "</p>"

    removePopups()

    var popup = new mapboxgl.Popup()
      .setLngLat(coordinates)
      .setHTML(html)
      .addTo(map)
    currentPopups.push(popup)
  });


  map.on('click', 'covid-dot', function (e) {
    var positive = e.features[0].properties.positive
    var total = e.features[0].properties.total
    var percentage = e.features[0].properties.percentage
    var lat = e.features[0].properties.lat
    var lon = e.features[0].properties.lon
    var zip = e.features[0].properties.zip
    var coordinates = e.features[0].geometry.coordinates.slice()

    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
      coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    var html = "<h1>" + zip + "</h1>"
    html += "<p>Positive: "+ positive + "</br>"
    html += "Total: "+ total + "</br>"
    html += "Percentage: "+ percentage + "%</p>"

    removePopups()

    var popup = new mapboxgl.Popup()
      .setLngLat(coordinates)
      .setHTML(html)
      .addTo(map)
    currentPopups.push(popup)
  });


  map.on('click', 'arrest-dot', function (e) {
    var id = e.features[0].properties.id
    var date = e.features[0].properties.date
    var lat = e.features[0].properties.lat
    var lon = e.features[0].properties.lon
    var desc = e.features[0].properties.desc_full
    var perp_age = e.features[0].properties.perp_age
    var perp_sex = e.features[0].properties.perp_sex
    var perp_race = e.features[0].properties.perp_race
    var coordinates = e.features[0].geometry.coordinates.slice()

    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
      coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    var html = "<strong>" + desc + "</strong>"
    html += "</br><i>" + date + "</i>"
    html += "<p>Age Range: "+ perp_age + "</br>"
    html += "Gender: "+ perp_sex + "</br>"
    html += "Race: "+ perp_race + "</p>"

    removePopups()

    var popup = new mapboxgl.Popup()
      .setLngLat(coordinates)
      .setHTML(html)
      .addTo(map)
    currentPopups.push(popup)
  });

  // Change cursor style on hover
  dot_styles = ["arrest-dot", "covid-dot", "tweet-dot"]
  for(d in dot_styles){
    map.on('mouseenter', dot_styles[d], function () {
      map.getCanvas().style.cursor = 'pointer';
    });
    map.on('mouseleave', dot_styles[d], function () {
      map.getCanvas().style.cursor = '';
    });
  }


});
